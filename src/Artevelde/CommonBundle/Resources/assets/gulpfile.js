'use strict';

var gulp   = require('gulp'),
	bower  = require('gulp-bower'),
	notify = require('gulp-notify'),
	rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
	sass   = require('gulp-ruby-sass'),

// Config
// ------
	paths = {
		bower: "./bower_components/",
        dist: "../public/",
        src: "./src/"
	};

// Tasks
// -----
/**
 * Run all tasks when 'gulp' is executed.
 */
gulp.task('default', ['css', 'js'], function() {
    // Default task must exist.
});

/**
 * Install Bower packages defined in bower.json into the given components folder.
 */
gulp.task('bower', function() {
	return bower()
		.pipe(gulp.dest(paths.bower));
});

/**
 * Create font files in destination folder.
 */
gulp.task('fonts', ['bower'], function() {
    gulp.src(paths.bower + 'fontawesome/f*/*.*')
        .pipe(gulp.dest(paths.dist));

    gulp.src(paths.bower + 'bootstrap-sass-official/assets/f*/**/*.*')
        .pipe(gulp.dest(paths.dist));
});

/**
 * Create CSS files in destination folder.
 */
gulp.task('css', ['bower', 'fonts'], function() {
	var configSass = {
            style: 'compressed', // nested, compact, compressed, expanded.
            loadPath: [
                paths.src   + 'css',
                paths.bower + 'bootstrap-sass-official/assets/stylesheets',
                paths.bower + 'fontawesome/scss'
            ]
        };

	return gulp.src(paths.src + 'css/*.scss', { base: paths.src })
        .pipe(rename({ suffix: '.min' }))
		.pipe(
            sass(configSass).on('error', notify.onError("Error: <%= error.message %>"))
        )
		.pipe(gulp.dest(paths.dist));
});

/**
 * Create JS files in destination folder.
 */
gulp.task('js', ['bower'], function() {

    // Bootstrap JS
    gulp.src(paths.bower + 'bootstrap-sass-official/assets/javascripts/bootstrap.js')
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.dist + 'js'));

    gulp.src(paths.src + 'js/*.js', { base: paths.src })
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(paths.dist))
});
