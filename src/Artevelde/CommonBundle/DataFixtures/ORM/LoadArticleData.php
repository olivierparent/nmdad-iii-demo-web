<?php

namespace Artevelde\CommonBundle\DataFixtures\ORM;

use Artevelde\CommonBundle\Entity\Article;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadArticleData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 2; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $articleA = new Article();
        $em->persist($articleA); // Manage Entity for persistence.
        $articleA
            ->setTitle('Test Artikel A')
            ->setBody('Lorem Ipsum A')
            ->setUser($this->getReference('TestUserB')); // Get reference from a previous Data Fixture.

        $articleB = new Article();
        $em->persist($articleB); // Manage Entity for persistence.
        $articleB
            ->setTitle('Test Artikel B')
            ->setBody('Lorem Ipsum B')
            ->setUser($this->getReference('TestUserA')); // Get reference from a previous Data Fixture.

        $em->flush(); // Persist all managed Entities.
    }
}
