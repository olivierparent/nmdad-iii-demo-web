<?php

namespace Artevelde\CommonBundle\DataFixtures\ORM;

use Artevelde\CommonBundle\Entity\User;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function hashPassword($user)
    {
        $factory = $this->container->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $hashedPassword = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        $user->setPassword($hashedPassword);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $userA = new User();
        $em->persist($userA); // Manage Entity for persistence.
        $userA
            ->setFirstName('Test User')
            ->setLastName('A')
            ->setUsername('testuserA')
            ->setPassword('testuserA');
        $this->hashPassword($userA);
        $this->addReference('TestUserA', $userA); // Reference for the next Data Fixture(s).

        $userB = new User();
        $em->persist($userB); // Manage Entity for persistence.
        $userB
            ->setFirstName('Test User')
            ->setLastName('B')
            ->setUsername('testuserB')
            ->setPassword('testuserB');
        $this->hashPassword($userB);
        $this->addReference('TestUserB', $userB); // Reference for the next Data Fixture(s).

        $em->flush(); // Persist all managed Entities.
    }
}
