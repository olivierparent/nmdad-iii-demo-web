<?php

namespace Artevelde\CommonBundle\DataFixtures\ORM;

use Artevelde\CommonBundle\Entity\Image;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadImageData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 3; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $imageA = new Image();
        $em->persist($imageA); // Manage Entity for persistence.
        $imageA
            ->setTitle('Test Image A')
            ->setUri('http://www.arteveldehogeschool.be/a')
            ->setUser($this->getReference('TestUserB')); // Get reference from a previous Data Fixture.

        $imageB = new Image();
        $em->persist($imageB); // Manage Entity for persistence.
        $imageB
            ->setTitle('Test Image B')
            ->setUri('http://www.arteveldehogeschool.be/b')
            ->setUser($this->getReference('TestUserA')); // Get reference from a previous Data Fixture.

        $em->flush(); // Persist all managed Entities.
    }
}
