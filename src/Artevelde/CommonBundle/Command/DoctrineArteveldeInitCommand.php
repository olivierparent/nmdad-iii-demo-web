<?php

namespace Artevelde\CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DoctrineArteveldeInitCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('doctrine:artevelde:init')
            ->setDescription('Creates database user, creates database and executes migration.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $app = $this->getApplication();
        $container = $this->getContainer();

        $name     = $container->getParameter('database_name');
        $user     = $container->getParameter('database_user');
        $password = $container->getParameter('database_password');

        $sql = "GRANT ALL PRIVILEGES ON {$name}.* TO '{$user}' IDENTIFIED BY '{$password}'";

        exec(sprintf('mysql --user=homestead --password=secret --execute="%s"', $sql));

        $app->find('doctrine:database:create')->run($input, $output);

        $container->get('doctrine')->getConnection()->close();

        $newInput = new ArrayInput([
            'command' => 'doctrine:migrations:migrate',
        ]);
        $newInput->setInteractive(false);
        $app->find('doctrine:migrations:migrate')->run($newInput, $output);
    }
}
