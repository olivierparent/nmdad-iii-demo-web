<?php

namespace Artevelde\CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DoctrineArteveldeResetCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('doctrine:artevelde:reset')
            ->setDescription('Drops database, creates database and executes migration.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $app = $this->getApplication();
        $container = $this->getContainer();

        $newInput = new ArrayInput([
            'command' => 'doctrine:database:drop',
            '--force' => true,
        ]);

        $app->find('doctrine:database:drop')->run(
            $newInput,
            $output
        );

        $app->find('doctrine:database:create')->run($input, $output);

        $container->get('doctrine')->getConnection()->close();

        $newInput = new ArrayInput([
            'command' => 'doctrine:migrations:migrate',
        ]);
        $newInput->setInteractive(false);
        $app->find('doctrine:migrations:migrate')->run($newInput, $output);
    }
}
