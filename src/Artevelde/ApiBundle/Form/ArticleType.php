<?php

namespace Artevelde\ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', null, array('required' => false))
            ->add('title')
            ->add('body')
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Artevelde\CommonBundle\Entity\Article',
            'csrf_protection' => false, // Disable CSRF for RESTful.
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'article'; // Form name.
    }
}
