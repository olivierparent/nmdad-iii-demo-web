<?php

namespace Artevelde\FrontOfficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserLoginType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', [
                'label' => 'Username',
                'attr'  => ['placeholder' => 'Enter your username.'],
            ])
            ->add('password', 'password', [
                'label' => 'Password',
                'attr'  => ['placeholder' => 'Enter your password.'],
            ])
            ->add('remember', 'checkbox', [
                'label' => 'Remember me',
                'data' => true,
            ])
            ->add('btn_login', 'submit', [
                'label' => 'Sign on',
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Artevelde\CommonBundle\Entity\User'
        ]);
    }

    /**
     * Name of the form.
     *
     * @return string
     */
    public function getName()
    {
        return 'artevelde_frontoffice_security_login_form'; // Form name.
    }
}
