<?php

namespace Artevelde\FrontOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        dump('Hello from the VarDumper Component (http://symfony.com/blog/new-in-symfony-2-6-vardumper-component) in the Debug Bar!');

        return [];
    }
}
