<?php

namespace Artevelde\FrontOfficeBundle\Controller;

use Artevelde\CommonBundle\Entity\User;
use Artevelde\FrontOfficeBundle\Form\UserLoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class SecurityController
 * @package Artevelde\FrontOfficeBundle\Controller
 *
 * @route("/security")
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login")
     * @Template()
     */
    public function loginAction(Request $request)
    {
        $entity = new User();
        $form = $this->createForm(new UserLoginType(), $entity, [
            'action' => $this->generateUrl('artevelde_frontoffice_security_check'),
            'method' => 'POST',
        ]);

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $session = $request->getSession();
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        /**
         * Return array with variables for Twig.
         */
        return [
            'form'  => $form->createView(),
            'error' => $error,
        ];
    }
}
