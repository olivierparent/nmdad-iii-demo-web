# language: en
Feature: Login Form
  In order to access secured content of the website
  As a user or an admin
  I need to log in

  Background:
    Given the users are:
    |username|password |role      |
    |user    |userpass |ROLE_USER |
    |admin   |adminpass|ROLE_ADMIN|

  @mink:default
  Scenario Outline: log in with valid credentials
    Given I am on the homepage
    When I follow "Run The Demo"
    Then I should be on "/demo/"
    When I follow "Access the secured area"
    Then I should be on "/demo/secured/login"
    When I fill in "username" with "<username>"
      And I fill in "password" with "<password>"
      And I press "Login"
    Then I should be on "/demo/secured/hello/World"
      And I should see "<text>"

# De reeds voorgedefineerde stappen in de context (MinkContext) vind je met:
# vagrant@homestead$ bin/behat -di
# vagrant@homestead$ bin/behat -dl

    Examples:
        |username|password |text              |
        |user    |userpass |logged in as user |
        |admin   |adminpass|logged in as admin|
