# language: nl
Functionaliteit: User Login
  Om toegang te krijgen tot de beveiligde inhoud van de website
  Als gebruiker
  Moet ik aanmelden
  
  Achtergrond:
    Stel dat de gebruikers de volgende gegevens bevat:
    |email                        |password|
    |olivier.parent@arteveldehs.be|testtest|

  Abstract Scenario: geldige aanmeldgegevens
    Stel ik ben op de aanmeldpagina
    Als ik een emailadres <email> ingeef
    En ik geef een wachtwoord <password> in
    Dan word ik aangemeld
    
    Voorbeelden:
    |email                        |password|logged in|
    |olivier.parent@arteveldehs.be|testtest|true     |
    |olivier.parent.arteveldehs.be|testtest|false    |
    
  Abstract Scenario: ongeldige aanmeldgegevens
    Stel ik ben op de aanmeldpagina
    Als ik een emailadres <email> ingeef
    En ik geef een wachtwoord <password> in
    Dan krijg ik een foutmelding
    
    Voorbeelden:
    |email                        |password|
    |olivier.parent@arteveldehs.be|testtest|
    |olivier.parent.arteveldehs.be|testtest|
        