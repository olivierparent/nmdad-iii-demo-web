New Media Design & Development III
==================================

I. Gegevens
-----------

 - `nmdad3_arteveldehogeschool_be`
 - `nmdad3_db_user`
 - `nmdad3_db_password`

II. Benodigdheden
-----------------

 - [AHS Laravel Homestead](https://bitbucket.org/olivierparent/homestead)

III. Installatie
----------------

### 1. Database

    vagrant@homestead$ db
    mysql> GRANT ALL PRIVILEGES ON nmdad3_arteveldehogeschool_be.*
        -> TO 'nmdad3_db_user' IDENTIFIED BY 'nmdad3_db_password';

### 2. Broncode

    vagrant@homestead$ sudo composer self-update

    vagrant@homestead$ mkdir ~/Code/nmdad-iii.arteveldehogeschool.be/
    vagrant@homestead$ cd ~/Code/nmdad-iii.arteveldehogeschool.be/
    vagrant@homestead$ git clone https://bitbucket.org/olivierparent/nmdad-iii-demo www/
    vagrant@homestead$ cd www/
    vagrant@homestead$ composer update
    vagrant@homestead$ php app/console doctrine:database:create
    vagrant@homestead$ php app/console doctrine:migrations:migrate

IV. Updates
-----------

    vagrant@homestead$ nmdad3
    vagrant@homestead$ php app/console assets:install --symlink --relative