<?php

namespace Acme\DemoBundle\Features\Context;

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;

/**
 * Behat context class.
 */
class FeatureContext extends RawMinkContext implements SnippetAcceptingContext
{
    use KernelDictionary;

    private $logger;
    private $security;
    private $session;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context object.
     * You can also pass arbitrary arguments to the context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @BeforeScenario
     */
    public function beforeScenario()
    {
        // Find containers with: $ php app/console container:debug
        $container = $this->getKernel()->getContainer(); // Requires KernelDictionary trait.
        $this->logger   = $container->get('logger');
        $this->security = $container->get('security.context');
        $this->session = $container->get('session');
    }

    /**
     * @Given the users are:
     */
    public function theUsersAre(TableNode $table)
    {
        // Do nothing.
    }

}
